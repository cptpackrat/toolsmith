'use strict'

const Command = require('./lib/command.js').Command

module.exports = function command (name) {
  return new Command(name)
}
