# API

## Command
Command objects describe the rules for parsing a command or subcommand, including any parameters, options or environment variables accepted or required by that command.

### command([name])
Returns a new command object, with an optional name. If `name` is not provided, the command will assume the name of the running process.

```js
// short form
require('toolsmith')()
  .option(...)
  .option(...)
  .parameter(...)
  .handler(...)
  .parse()

// long form
const command = require('toolsmith')
command()
  .option(...)
  .option(...)
  .subcommand(command('foo')
    .option(...)
    .parameter(...)
    .parameter(...))
    .handler(...)
  .subcommand(command('bar')
    .option(...)
    .handler(...))
  .parse()
```

### .error([fn])
Installs a handler function that will be called whenever an error message is printed. This includes errors that occur during argument parsing, and anything printed via `Context.error`, `Context.fatal` or `Context.usage` from within a handler or middleware function.

The default handler is `console.error`, only one handler can be set at a time, and any existing handler (including the default) can be removed by specifying `null`. If no handler is installed then error messages will be silently discarded.

```js
require('toolsmith')()
  .error((msg) => {
    console.error('ERROR: %s', msg)
  })
  .parse()
```

```
$ ./example.js --nope
ERROR: example.js: Unknown option '--nope'.
ERROR: example.js: Type 'example.js --help' for more information.
```

### .exit([fn])
Installs a handler function that will be called whenever a process exit is requested. This includes errors during argument parsing, auto-help attempting to exit after displaying a message, and `Context.exit`, `Context.fatal` or `Context.usage` being called from within a handler or middleware function.

The default handler is `process.exit`, only one handler can be set at a time, and any existing handler (including the default) can be removed by specifying `null`. If no handler is installed then exit requests will be ignored.

```js
require('toolsmith')()
  .exit((code) => {
    console.error('got exit code %d!', code)
    process.exit(code)
  })
  .parse()
```

```
$ ./example.js --nope
example.js: Unknown option '--nope'.
example.js: Type 'example.js --help' for more information.
got exit code 1!
```

### .handler(fn)
Installs a handler function that will be called when the associated command is executed. The handler will receive a single argument, a context object representing the current invocation of the command:

```js
require('toolsmith')()
  .handler((ctx) => {
    console.log('greetings from %s!', ctx.name)
  })
  .parse()
```

```
$ ./example.js
greetings from example.js!
```

__Note__: Any command object that does not have subcommands _must_ have a handler associated with it. A command may have both subcommands and a handler, in which case the handler will only be called if the command is executed without any arguments.

### .install(fn)
Convenience function that executes the provided function, passing the command option as the first argument. This method is intended for installing command components via `require`:

```js
// index.js
require('toolsmith')()
  .install(require('./cli/config.js'))
  .handler((ctx) => {
    console.log('greetings from %s', ctx.name)
  })
  .parse()
```

```js
// cli/config.js
module.exports = (cmd) => {
  cmd
    .option({
      long: 'config',
      desc: 'Specify a custom config path.'
    })
    .variable({
      key: 'config',
      name: 'EXAMPLE_CONFIG',
      desc: 'Specify a custom config path.'
    })
    .middleware((ctx, next) => {
      console.log("loading %s", ctx.opts.config || ctx.vars.config || '~/.examplerc')
      next()
    })
}
```

```
$ ./example.js
loading ~/.examplerc
greetings from example.js

./example.js --config=foo.conf
loading foo.conf
greetings from example.js

EXAMPLE_CONFIG=/etc/foo.conf ./example.js
loading /etc/foo.conf
greetings from example.js
```

### .middleware(fn)
Installs a handler function that will be called after all arguments have been parsed, but before any command handler is executed. Multiple middleware handlers can be installed, and will be executed in the order they are installed. Subcommands will execute any middleware belonging to their parents before executing their own. The middleware handler will receive two arguments, a context object representing the current invocation of the command, and a continuation function:

```js
require('toolsmith')()
  .middleware((ctx, next) => {
    console.log("ME FIRST")
    next()
  })
  .handler((ctx) => {
    console.log("ok jeez chill")
  })
  .parse()
```

```
$ ./example.js
ME FIRST
ok jeez chill
```

### .option(opts)
Registers an option or flag that the command will accept. Options must have a long form (e.g. `--arg`), may also have a short form (e.g. `-a`), and may or may not accept a value (e.g. `--arg=VAL` or `-aVAL`).

```js
require('toolsmith')()
  .option({
    long: 'foo'
  })
  .option({
    long: 'bar',
    short: 'B',
    value: true
  })
  .handler((ctx) => {
    console.log(ctx.opts)
  })
  .parse()
```

```
$ ./example.js --foo -Bsomething
{ foo: true, bar: 'something' }
```

Valid `opts` keys include:

  - `desc`: Optional string describing the purpose of the option. This string will be displayed in help output.
  - `inherit`: Optional boolean value indicating whether or not this option should be inherited by sub-commands.
  - `key`: Optional string that will be used as the key for storing any collected results. If omitted, the long form will be used instead.
  - `long`: Mandatory long form of the option; must be a string at least two characters long.
  - `repeatable`: Optional boolean value indicating whether or not this option can occur multiple times.
  - `short`: Optional short form of the option; must be a string exactly one character long.
  - `value`: Optional boolean value indicating whether or not this option receives a value.

The manner in which options are recorded depends on how they are configured:

  - If an option is neither repeatable nor accepts a value, it will be recorded as `true`.
  - If an option is repeatable, it will be recorded as an integer count of the number of times it occured.
  - If an option accepts a value, it will be recorded as the collected value.
  - If an option is both repeatable and accepts a value, an array of collected values will be recorded.
  - In all cases, no value is recorded for an option that occurs zero times.

If a string is provided is provided as the `value` key, it will be used to describe the value in help output:

```js
require('toolsmith')()
  .option({
    long: 'foo',
    short: 'f',
    value: 'FOO',
    desc: 'Specify custom foo.'
  })
  .parse()
```

```
$ ./example.js --help
Usage: example.js [OPTIONS...] <bar>
Options:
  -h,--help     You are here.
  -f,--foo=FOO  Specify custom foo.
```

### .parameter(opts)
Registers a named positional parameter that the command will accept. Parameters may be optional, may accept multiple values, and are collected in the order they are specified.

```js
require('toolsmith')()
  .parameter({
    name: 'foo'
  })
  .parameter({
    name: 'bar',
    variadic: true
  })
  .handler((ctx) => {
    console.log(ctx.args)
  })
  .parse()
```

```
$ ./example.js one two three four
[ foo: 'one', bar: [ 'two', 'three', 'four' ] ]

$ ./example.js one two -- three four
[ 'three', 'four', foo: 'one', bar: [ 'two' ] ]
```

Valid `opts` keys include:

  - `desc`: Optional string describing the purpose of the parameter. This string will be displayed in help output.
  - `key`: Optional string that will be used as the key for storing any collected results. If omitted, the parameter name will be used instead.
  - `name`: Mandatory parameter name, used in help output.
  - `optional`: Optional boolean value indicating whether or not this parameter can be omitted. Any optional parameters must come last, they cannot be followed by a required parameter.
  - `variadic`: Optional boolean value indicating whether or not this parameter will accept multiple values. Only the final parameter may be variadic.

__Note__: Named parameters and subcommands are mutually exclusive.

### .parse([argv])
Parses the specified list of arguments according to the rules installed in the command object. If `argv` is ommitted, `process.argv.slice(2)` will be used instead.

Executes any installed middleware, followed by the handler function registered for the target command. A context object containing any collected parameters, options or environment variables is passed as the first parameter to each function executed.

### .subcommand(cmd)
Installs one command object as a subcommand to another. If present, the first argument to the parent command will be interpreted as the name of the subcommand to execute.

```js
const command = require('toolsmith')
command()
  .handler((ctx) => {
    console.log('greetings from parent command')
  })
  .subcommand(command('sub')
    .handler((ctx) => {
      console.log('greetings from subcommand')
    }))
  .parse()
```

```
$ ./example.js
greetings from parent command

$ ./example.js sub
greetings from subcommand
```

__Note__: Subcommands and named parameters are mutually exclusive.

### .summary(desc)
Adds a string describing the function of a command. This string will be displayed in help output, either as the primary command summary or as part of a sub-command listing:

```js
const command = require('toolsmith')
command()
  .summary('Does the primary thing, with gusto!')
  .subcommand(command('also')
    .summary('Does secondary thing, with comparable enthusiasm!'))
  .parse()
```

```
$ ./example.js --help
Usage: example.js [OPTIONS...]
Summary:
  Does the primary thing, with gusto!
Commands:
  also  Does secondary thing, with comparable enthusiasm!
Options:
  -h,--help  You are here.

$ .example.js also --help
Usage: example.js also [OPTIONS...]
Summary:
  Does secondary thing, with comparable enthusiasm!
Options:
  -h,--help  You are here.
```

### .variable(opts)
Registers an environment variable to be collected by the command.

```js
require('toolsmith')()
  .variable({
    name: 'EXAMPLE_VAR1'
  })
  .variable({
    key: 'var2',
    name: 'EXAMPLE_VAR2'
  })
  .handler((ctx) => {
    console.log(ctx.vars)
  })
  .parse()
```

```
$ EXAMPLE_VAR1=foo EXAMPLE_VAR2=bar ./example.js
{ EXAMPLE_VAR1: 'foo', var2: 'bar' }
```

Valid `opts` keys include:

  - `default`: Optional default value to be used if the variable is not set. The presence of a default value will satisfy a variable marked as required.
  - `desc`: Optional string describing the purpose of the variable. This string will be displayed in help output.
  - `inherit`: Optional boolean value indicating whether or not this variable should be inherited by subcommands.
  - `key`: Optional string that will be used as the key for storing any collected results. If omitted, the variable name will be used instead.
  - `name`: Mandatory environment variable name.
  - `required`: Optional boolean value indicating whether or not the variable must be set.

## Context
Context objects are created during argument parsing and populated with any collected paramters, options and environment variables. Any middleware or command handlers executed during a call to `Command.parse` will be passed a context object as their first parameter. The context object also exposes some utility functions for error handling.

### .args
The full list of any positional parameters collected during parsing.

### .error(...args)
Print an error message to the current command's error message handler (installed via `Command.error`; `console.error` by default.) Arguments to this function are formatted according to the rules of `util.format`.

### .exit(code)
Request to exit via the current command's exit handler (installed via `Command.exit`; `process.exit` by default) with the specified exit code.

### .fatal(code, ...args)
Print an error message and then exit with the specified code, equivalent to calling `Context.error(...args)` followed by `Context.exit(code)`.

### .help()
Print an auto-generated help page, then exit with code 0.

### .name
The name of the currently executing command.

### .opts
The full list of any options collected during parsing.

### .path
The full name of the currently executing command, including any parent commands.

### .usage(code, ...args)
Similar to `Command.fatal`, but prints an additional message directing the user to try the `--help` flag for assistance.

### .vars
The full list of any environment variables collected during parsing.

