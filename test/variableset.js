'use strict'

/* eslint-disable no-new */
/* eslint-disable no-unused-expressions */

const util = require('./util.js')
const chai = require('chai')
const expect = chai.expect

const VariableSet = require('../lib/variableset.js')

describe('variableset', () => {
  describe('push', () => {
    it('accepts variable details as an object', () => {
      var set = new VariableSet()
      set.push({
        name: 'FOO',
        inherit: false,
        required: false,
        default: 'bar',
        desc: 'Foo.'
      })
      expect(set[0]).to.deep.equal({
        key: 'FOO',
        name: 'FOO',
        inherit: false,
        required: false,
        default: 'bar',
        desc: 'Foo.'
      })
    })
    it('unrecognised fields are ignored', () => {
      var set = new VariableSet()
      set.push({
        name: 'FOO',
        herp: 'herp',
        inherit: false,
        required: false,
        default: 'bar',
        desc: 'Foo.',
        derp: 'derp'
      })
      expect(set[0]).to.deep.equal({
        key: 'FOO',
        name: 'FOO',
        inherit: false,
        required: false,
        default: 'bar',
        desc: 'Foo.'
      })
    })
    it('requires a variable name', () => {
      var set = new VariableSet()
      expect(() => {
        set.push({})
      }).to.throw()
    })
    it('requires only a variable name', () => {
      var set = new VariableSet()
      set.push({ name: 'FOO' })
    })
    it('expects name to be a string', () => {
      var set = new VariableSet()
      util.forEachNonStr(false, (val) => {
        expect(() => {
          set.push({ name: val })
        }).to.throw()
      })
    })
    it('expects name to be unique', () => {
      var set = new VariableSet()
      set.push({ name: 'FOO' })
      expect(() => {
        set.push({ name: 'FOO' })
      }).to.throw()
    })
    it('expects default value to be a string', () => {
      var set = new VariableSet()
      util.forEachNonStr(true, (val) => {
        expect(() => {
          set.push({ name: 'FOO', default: val })
        }).to.throw()
      })
    })
    it('expects description to be a string', () => {
      var set = new VariableSet()
      util.forEachNonStr(true, (val) => {
        expect(() => {
          set.push({ name: 'FOO', desc: val })
        }).to.throw()
      })
    })
    it('coerces \'required\' and \'inherit\' into booleans', () => {
      var set = new VariableSet()
      set.push({
        name: 'FOO',
        required: 1
      })
      expect(set[0].required).to.equal(true)
      expect(set[0].inherit).to.equal(false)
    })
    it('uses custom key if one is provided', () => {
      var set = new VariableSet()
      set.push({
        key: 'foo',
        name: 'FOO'
      })
      expect(set[0].key).to.equal('foo')
      expect(set[0].name).to.equal('FOO')
    })
    it('expects key to be a string', () => {
      var set = new VariableSet()
      util.forEachNonStr(true, (val) => {
        expect(() => {
          set.push({ name: 'FOO', key: val })
        }).to.throw()
      })
    })
    it('expects key to be unique', () => {
      var set = new VariableSet()
      set.push({ key: 'foo', name: 'FOO' })
      expect(() => {
        set.push({ key: 'foo', name: 'BAR' })
      }).to.throw()
    })
  })
  describe('clone', () => {
    it('returns an exact copy of the set', () => {
      var set = new VariableSet()
      set.push({ name: 'FOO' })
      set.push({ name: 'BAR' })
      expect(set.clone()).to.not.equal(set)
      expect(set.clone()).to.deep.equal(set)
    })
  })
  describe('inherit', () => {
    it('appends the contents of a parent set', () => {
      var var1 = { name: 'FOO', inherit: true }
      var var2 = { name: 'BAR', inherit: true }
      var set1 = new VariableSet()
      var set2 = new VariableSet()
      set1.push(var1)
      set2.push(var2)
      set2.inherit(set1)
      var exp = new VariableSet()
      exp.push(var2)
      exp.push(var1)
      expect(set2).to.deep.equal(exp)
    })
    it('skips variable that are not marked as inheritable', () => {
      var var1 = { name: 'FOO', inherit: false }
      var var2 = { name: 'BAR', inherit: true }
      var set1 = new VariableSet()
      var set2 = new VariableSet()
      set1.push(var1)
      set1.push(var2)
      set2.inherit(set1)
      var exp = new VariableSet()
      exp.push(var2)
      expect(set2).to.deep.equal(exp)
    })
    it('skips variable that conflict with existing variable names', () => {
      var var1 = { name: 'FOO', inherit: true }
      var var2 = { name: 'FOO', inherit: true }
      var set1 = new VariableSet()
      var set2 = new VariableSet()
      set1.push(var1)
      set2.push(var2)
      set2.inherit(set1)
      var exp = new VariableSet()
      exp.push(var2)
      expect(set2).to.deep.equal(exp)
    })
    it('skips variable that conflict with existing variable keys', () => {
      var var1 = { key: 'foo', name: 'FOO', inherit: true }
      var var2 = { key: 'foo', name: 'BAR', inherit: true }
      var set1 = new VariableSet()
      var set2 = new VariableSet()
      set1.push(var1)
      set2.push(var2)
      set2.inherit(set1)
      var exp = new VariableSet()
      exp.push(var2)
      expect(set2).to.deep.equal(exp)
    })
  })
  describe('help', () => {
    it('displays variable names', () => {
      var set = new VariableSet()
      set.push({ name: 'FOO' })
      set.push({ name: 'BAR' })
      expect(set.help()).to.equal([
        'Variables:',
        '  FOO',
        '  BAR'
      ].join('\n'))
    })
    it('displays description if one is provided', () => {
      var set = new VariableSet()
      set.push({ name: 'FOO', desc: 'Source foo.' })
      set.push({ name: 'BAR', desc: 'Destination bar.' })
      expect(set.help()).to.equal([
        'Variables:',
        '  FOO  Source foo.',
        '  BAR  Destination bar.'
      ].join('\n'))
    })
    it('pads descriptions for a consistent start location', () => {
      var set = new VariableSet()
      set.push({ name: 'FOO', desc: 'Source foo.' })
      set.push({ name: 'BAAAR', desc: 'Destination bar.' })
      expect(set.help()).to.equal([
        'Variables:',
        '  FOO    Source foo.',
        '  BAAAR  Destination bar.'
      ].join('\n'))
      set.push({ name: 'BOOOOOO', desc: 'Additional boo.' })
      expect(set.help()).to.equal([
        'Variables:',
        '  FOO      Source foo.',
        '  BAAAR    Destination bar.',
        '  BOOOOOO  Additional boo.'
      ].join('\n'))
    })
  })
})
