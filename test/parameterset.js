'use strict'

/* eslint-disable no-new */
/* eslint-disable no-unused-expressions */

const util = require('./util.js')
const chai = require('chai')
const expect = chai.expect

const ParameterSet = require('../lib/parameterset.js')

describe('parameterset', () => {
  describe('push', () => {
    it('accepts parameter details as an object', () => {
      var set = new ParameterSet()
      set.push({
        name: 'foo',
        optional: true,
        variadic: false,
        desc: 'Foo.'
      })
      expect(set[0]).to.deep.equal({
        key: 'foo',
        name: 'foo',
        optional: true,
        variadic: false,
        desc: 'Foo.'
      })
    })
    it('unrecognised fields are ignored', () => {
      var set = new ParameterSet()
      set.push({
        name: 'foo',
        herp: 'herp',
        optional: true,
        variadic: false,
        desc: 'Foo.',
        derp: 'derp'
      })
      expect(set[0]).to.deep.equal({
        key: 'foo',
        name: 'foo',
        optional: true,
        variadic: false,
        desc: 'Foo.'
      })
    })
    it('requires a parameter name', () => {
      var set = new ParameterSet()
      expect(() => {
        set.push({})
      }).to.throw()
    })
    it('requires only a parameter name', () => {
      var set = new ParameterSet()
      set.push({ name: 'foo' })
    })
    it('expects name to be a string', () => {
      var set = new ParameterSet()
      util.forEachNonStr(false, (val) => {
        expect(() => {
          set.push({ name: val })
        }).to.throw()
      })
    })
    it('expects description to be a string', () => {
      var set = new ParameterSet()
      util.forEachNonStr(true, (val) => {
        expect(() => {
          set.push({ name: 'foo', desc: val })
        }).to.throw()
      })
    })
    it('coerces \'optional\' and \'variadic\' into booleans', () => {
      var set = new ParameterSet()
      set.push({
        name: 'foo',
        optional: 1
      })
      expect(set[0].optional).to.equal(true)
      expect(set[0].variadic).to.equal(false)
    })
    it('uses custom key if one is provided', () => {
      var set = new ParameterSet()
      set.push({
        key: 'bar',
        name: 'foo',
        optional: true,
        variadic: false,
        desc: 'Foo.'
      })
      expect(set[0].key).to.equal('bar')
      expect(set[0].name).to.equal('foo')
    })
    it('expects key to be a string', () => {
      var set = new ParameterSet()
      util.forEachNonStr(true, (val) => {
        expect(() => {
          set.push({ name: 'foo', key: val })
        }).to.throw()
      })
    })
    it('expects key to be unique', () => {
      var set = new ParameterSet()
      set.push({ key: 'foo', name: 'foo' })
      expect(() => {
        set.push({ key: 'foo', name: 'bar' })
      }).to.throw()
    })
    it('only allows optional parameters at the end', () => {
      var set = new ParameterSet()
      set.push({ name: 'foo', optional: true })
      expect(() => {
        set.push({ name: 'bar', optional: false })
      }).to.throw()
    })
    it('only allows one parameter to be variadic', () => {
      var set = new ParameterSet()
      set.push({ name: 'foo', variadic: true })
      expect(() => {
        set.push({ name: 'bar', variadic: true })
      }).to.throw()
    })
    it('only allows the final parameter to be variadic', () => {
      var set = new ParameterSet()
      set.push({ name: 'foo', variadic: true })
      expect(() => {
        set.push({ name: 'bar' })
      }).to.throw()
    })
  })
  describe('usage', () => {
    it('uses angle brackets to indicate a parameter is required', () => {
      var set = new ParameterSet()
      set.push({ name: 'foo' })
      set.push({ name: 'bar' })
      expect(set.usage()).to.equal('<foo> <bar>')
    })
    it('uses square brackets to indicate a parameter is optional', () => {
      var set = new ParameterSet()
      set.push({ name: 'foo', optional: true })
      set.push({ name: 'bar', optional: true })
      expect(set.usage()).to.equal('[foo] [bar]')
    })
    it('uses ellipsis to indicate a required parameter is variadic', () => {
      var set = new ParameterSet()
      set.push({ name: 'foo', variadic: true })
      expect(set.usage()).to.equal('<foo...>')
    })
    it('uses ellipsis to indicate an optional parameter is variadic', () => {
      var set = new ParameterSet()
      set.push({ name: 'foo', optional: true, variadic: true })
      expect(set.usage()).to.equal('[foo...]')
    })
  })
  describe('help', () => {
    it('displays parameter name', () => {
      var set = new ParameterSet()
      set.push({ name: 'foo' })
      set.push({ name: 'bar' })
      expect(set.help()).to.equal([
        'Parameters:',
        '  foo',
        '  bar'
      ].join('\n'))
    })
    it('displays description if one is provided', () => {
      var set = new ParameterSet()
      set.push({ name: 'foo', desc: 'Source foo.' })
      set.push({ name: 'bar', desc: 'Destination bar.' })
      expect(set.help()).to.equal([
        'Parameters:',
        '  foo  Source foo.',
        '  bar  Destination bar.'
      ].join('\n'))
    })
    it('pads descriptions for a consistent start location', () => {
      var set = new ParameterSet()
      set.push({ name: 'foo', desc: 'Source foo.' })
      set.push({ name: 'baaar', desc: 'Destination bar.' })
      expect(set.help()).to.equal([
        'Parameters:',
        '  foo    Source foo.',
        '  baaar  Destination bar.'
      ].join('\n'))
      set.push({ name: 'boooooo', desc: 'Additional boo.' })
      expect(set.help()).to.equal([
        'Parameters:',
        '  foo      Source foo.',
        '  baaar    Destination bar.',
        '  boooooo  Additional boo.'
      ].join('\n'))
    })
  })
})
