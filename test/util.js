'use strict'

exports.forEachNonStr = function forEachNonStr (opt, fn) {
  [undefined, null, 0, 1, true, false, [], {}, () => {}]
    .slice(opt ? 2 : 0)
    .forEach(fn)
}
