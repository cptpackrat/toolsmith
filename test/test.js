'use strict'

/* eslint-disable no-new */
/* eslint-disable no-unused-expressions */

const path = require('path')
const chai = require('chai')
const expect = chai.expect

const lib = require('../lib/command.js')
const command = require('..')
const Command = lib.Command
const CommandSet = lib.CommandSet
const Context = lib.Context

describe('index', () => {
  it('returns a command object', () => {
    expect(command()).to.be.instanceof(Command)
  })
})

describe('command', () => {
  it('accepts an optional command name', () => {
    new Command()
    new Command('foo')
  })
  it('expects command name to be a string', () => {
    forEachNonStr(true, (val) => {
      expect(() => new Command(val)).to.throw()
    })
  })
})

describe('command.summary', () => {
  it('returns a command object', () => {
    expect(new Command().summary('foo')).to.be.instanceof(Command)
  })
  it('expects description to be a string', () => {
    forEachNonStr(false, (val) => {
      expect(() => new Command().summary(val)).to.throw()
    })
  })
})

describe('command.handler', () => {
  it('returns a command object', () => {
    expect(new Command().handler(() => {})).to.be.instanceof(Command)
  })
  it('expects handler to be a function', () => {
    forEachNonFn(false, (val) => {
      expect(() => new Command().handler(val)).to.throw()
    })
  })
})

describe('command.option', () => {
  it('returns a command object', () => {
    expect(new Command().option({ long: 'foo' })).to.be.instanceof(Command)
  })
  it('requires a long option', () => {
    expect(() => {
      new Command().option({})
    }).to.throw()
  })
  it('rejects the use of \'help\' as a long option', () => {
    expect(() => {
      new Command().option({ long: 'help' })
    }).to.throw()
  })
  it('rejects the use of \'h\' as a short option', () => {
    expect(() => {
      new Command().option({ long: 'foo', short: 'h' })
    }).to.throw()
  })
  it('rejects the use of \'help\' as an option key', () => {
    expect(() => {
      new Command().option({ key: 'help', long: 'foo' })
    }).to.throw()
  })
})

describe('command.parameter', () => {
  it('returns a command object', () => {
    expect(new Command().parameter({ name: 'foo' })).to.be.instanceof(Command)
  })
  it('requires a parameter name', () => {
    expect(() => {
      new Command().parameter({})
    }).to.throw()
  })
})

describe('command.subcommand', () => {
  it('returns a command object', () => {
    expect(new Command().subcommand(new Command('foo'))).to.be.instanceof(Command)
  })
  it('expects subcommand to be a command object', () => {
    forEachPrimitive(false, (val) => {
      expect(() => new Command().subcommand(val)).to.throw()
    })
  })
  it('expects subcommand to be named', () => {
    expect(() => new Command().subcommand(new Command())).to.throw()
  })
  it('expects subcommand names to be unique', () => {
    expect(() => {
      new Command()
        .subcommand(new Command('foo'))
        .subcommand(new Command('foo'))
    }).to.throw()
  })
  it('accepts subcommands with existing parents', () => {
    var foo = new Command('foo')
    new Command().subcommand(foo)
    new Command().subcommand(foo)
  })
})

describe('command.middleware', () => {
  it('returns a command object', () => {
    expect(new Command().middleware(() => {})).to.be.instanceof(Command)
  })
  it('expects handler to be a function', () => {
    forEachNonFn(false, (val) => {
      expect(() => new Command().middleware(val)).to.throw()
    })
  })
})

describe('command.error', () => {
  it('returns a command object', () => {
    expect(new Command().error(() => {})).to.be.instanceof(Command)
  })
  it('expects handler to be a function or null', () => {
    new Command().error()
    forEachNonFn(true, (val) => {
      expect(() => new Command().error(val)).to.throw()
    })
  })
})

describe('command.exit', () => {
  it('returns a command object', () => {
    expect(new Command().exit(() => {})).to.be.instanceof(Command)
  })
  it('expects handler to be a function or null', () => {
    new Command().exit()
    forEachNonFn(true, (val) => {
      expect(() => new Command().exit(val)).to.throw()
    })
  })
})

describe('command.install', () => {
  it('returns a command object', () => {
    expect(new Command().install(() => {})).to.be.instanceof(Command)
  })
  it('expects a function', () => {
    forEachNonFn(false, (val) => {
      expect(() => new Command().install(val)).to.throw()
    })
  })
  it('executes function with itself as an argument', () => {
    var done = false
    new Command().install((cmd) => {
      expect(cmd).to.be.instanceof(Command)
      done = true
    })
    expect(done).to.equal(true)
  })
})

describe('commandset.push', () => {
  it('expects command to have a name', () => {
    var set = new CommandSet()
    expect(() => {
      set.push(new Command())
    }).to.throw()
  })
  it('expects command names to be unique', () => {
    var set = new CommandSet()
    expect(() => {
      set.push(new Command('foo'))
      set.push(new Command('foo'))
    }).to.throw()
  })
  it('builds a list of command names', () => {
    var set = new CommandSet()
    var foo = new Command('foo')
    var bar = new Command('bar')
    set.push(foo)
    set.push(bar)
    expect(set.name).to.deep.equal({
      foo: foo,
      bar: bar
    })
  })
})

describe('commandset.usage', () => {
  it('returns commands as a pipe-separated list', () => {
    var set = new CommandSet()
    set.push(new Command('foo'))
    expect(set.usage()).to.equal('foo')
    set.push(new Command('bar'))
    set.push(new Command('boo'))
    expect(set.usage()).to.equal('foo|bar|boo')
  })
})

describe('commandset.help', () => {
  it('displays command names', () => {
    var set = new CommandSet()
    set.push(new Command('foo'))
    set.push(new Command('bar'))
    expect(set.help()).to.equal([
      'Commands:',
      '  foo',
      '  bar'
    ].join('\n'))
  })
  it('displays command description if one is provided', () => {
    var set = new CommandSet()
    set.push(new Command('foo').summary('Perform foo.'))
    set.push(new Command('bar').summary('Perform bar.'))
    expect(set.help()).to.equal([
      'Commands:',
      '  foo  Perform foo.',
      '  bar  Perform bar.'
    ].join('\n'))
  })
  it('pads descriptions for a consistent start location', () => {
    var set = new CommandSet()
    set.push(new Command('foo').summary('Perform foo.'))
    set.push(new Command('baaar').summary('Perform bar.'))
    expect(set.help()).to.equal([
      'Commands:',
      '  foo    Perform foo.',
      '  baaar  Perform bar.'
    ].join('\n'))
    set.push(new Command('boooooo').summary('Perform boo.'))
    expect(set.help()).to.equal([
      'Commands:',
      '  foo      Perform foo.',
      '  baaar    Perform bar.',
      '  boooooo  Perform boo.'
    ].join('\n'))
  })
})

describe('context', () => {
  it('knows the command name', () => {
    var ctx = new Context(new Command('foo'))
    expect(ctx.name).to.equal('foo')
    expect(ctx.path).to.equal('foo')
  })
  it('knows the command path ', () => {
    var ctx = new Context(new Command('foo'))._push(new Command('bar'))
    expect(ctx.name).to.equal('bar')
    expect(ctx.path).to.equal('foo bar')
  })
  it('picks a name if the command doesn\'t have one', () => {
    var exp = path.basename(process.argv[1])
    var ctx = new Context(new Command())
    expect(ctx.name).to.equal(exp)
    expect(ctx.path).to.equal(exp)
  })
})

describe('context.exit', () => {
  it('calls process.exit by default', () => {
    expect(withDecoy('process.exit', null, () => {
      new Context(new Command()).exit()
    })).to.equal(true)
  })
  it('calls exit handler if one is set', () => {
    var done = false
    new Context(new Command().exit(() => {
      done = true
    })).exit()
    expect(done).to.equal(true)
  })
  it('calls nothing if exit handler is set to null', () => {
    expect(withDecoy('process.exit', null, () => {
      new Context(new Command().exit(null)).exit()
    })).to.equal(false)
  })
  it('passes exit code to the handler', () => {
    var done = false
    new Context(new Command().exit((code) => {
      expect(code).to.equal(123)
      done = true
    })).exit(123)
    expect(done).to.equal(true)
  })
  it('passes zero if no code was specified', () => {
    var done = false
    new Context(new Command().exit((code) => {
      expect(code).to.equal(0)
      done = true
    })).exit()
    expect(done).to.equal(true)
  })
  it('searches parent commands for an exit handler', () => {
    var done = false
    new Context(new Command('foo')
      .exit(() => { done = true }))
      ._push(new Command('bar'))
      .exit()
    expect(done).to.equal(true)
  })
  it('stops searching at a null handler', () => {
    var done = false
    new Context(new Command('foo')
      .exit(() => { done = true }))
      ._push(new Command('bar')
        .exit(null))
      ._push(new Command('boo'))
      .exit()
    expect(done).to.equal(false)
  })
  it('calls the first handler found', () => {
    var done1 = false
    var done2 = false
    new Context(new Command('foo')
      .exit(() => { done1 = true }))
      ._push(new Command('bar')
        .exit(() => { done2 = true }))
      ._push(new Command('boo'))
      .exit()
    expect(done1).to.equal(false)
    expect(done2).to.equal(true)
  })
})

describe('context.error', () => {
  it('calls console.error by default', () => {
    expect(withDecoy('console.error', null, () => {
      new Context(new Command()).error()
    })).to.equal(true)
  })
  it('calls message handler if one is set', () => {
    var done = false
    new Context(new Command().error(() => {
      done = true
    })).error()
    expect(done).to.equal(true)
  })
  it('calls nothing if message handler is set to null', () => {
    expect(withDecoy('console.error', null, () => {
      new Context(new Command().error(null)).error()
    })).to.equal(false)
  })
  it('passes message to the handler', () => {
    var done = false
    new Context(new Command('foo').error((msg) => {
      expect(msg).to.equal('foo: bar')
      done = true
    })).error('bar')
    expect(done).to.equal(true)
  })
  it('searches parent commands for a message handler', () => {
    var done = false
    new Context(new Command('foo')
      .error(() => { done = true }))
      ._push(new Command('bar'))
      .error()
    expect(done).to.equal(true)
  })
  it('stops searching at a null handler', () => {
    var done = false
    new Context(new Command('foo')
      .error(() => { done = true }))
      ._push(new Command('bar')
        .error(null))
      ._push(new Command('boo'))
      .error()
    expect(done).to.equal(false)
  })
  it('calls the first handler found', () => {
    var done1 = false
    var done2 = false
    new Context(new Command('foo')
      .error(() => { done1 = true }))
      ._push(new Command('bar')
        .error(() => { done2 = true }))
      ._push(new Command('boo'))
      .error()
    expect(done1).to.equal(false)
    expect(done2).to.equal(true)
  })
})

describe('context.fatal', () => {
  it('passes error message to the error handler', () => {
    var done1 = false
    var done2 = false
    new Context(new Command('foo')
      .error((msg) => {
        expect(msg).to.equal('foo: bar boo')
        done1 = true
      })
      .exit((code) => {
        expect(code).to.equal(1)
        done2 = true
      })).fatal('bar %s', 'boo')
    expect(done1).to.equal(true)
    expect(done2).to.equal(true)
  })
  it('passes optional exit code to the exit handler', () => {
    var done1 = false
    var done2 = false
    new Context(new Command('foo')
      .error((msg) => {
        expect(msg).to.equal('foo: bar boo')
        done1 = true
      })
      .exit((code) => {
        expect(code).to.equal(123)
        done2 = true
      })).fatal(123, 'bar %s', 'boo')
    expect(done1).to.equal(true)
    expect(done2).to.equal(true)
  })
})

describe('context.usage', () => {
  it('passes error message to the error handler', () => {
    var done = false
    var errors = []
    new Context(new Command('foo')
      .error((msg) => {
        errors.push(msg)
      })
      .exit((code) => {
        expect(code).to.equal(1)
        done = true
      })).usage('bar %s', 'boo')
    expect(done).to.equal(true)
    expect(errors).to.deep.equal([
      'foo: bar boo',
      'foo: Type \'foo --help\' for more information.'
    ])
  })
  it('passes optional exit code to the exit handler', () => {
    var done = false
    var errors = []
    new Context(new Command('foo')
      .error((msg) => {
        errors.push(msg)
      })
      .exit((code) => {
        expect(code).to.equal(123)
        done = true
      })).usage(123, 'bar %s', 'boo')
    expect(done).to.equal(true)
    expect(errors).to.deep.equal([
      'foo: bar boo',
      'foo: Type \'foo --help\' for more information.'
    ])
  })
})

describe('context.help', () => {
  it('passes help message to console.log', () => {
    expect(withDecoyLogs(() => {
      new Context(new Command('foo')
        .exit(null))
        .help()
    })).to.equal([
      'Usage: foo [OPTIONS...]',
      'Options:',
      '  -h,--help  You are here.'
    ].join('\n'))
  })
  it('passes code zero to the exit handler', () => {
    var done = false
    withDecoyLogs(() => {
      new Context(new Command()
        .exit((code) => {
          expect(code).to.equal(0)
          done = true
        })).help()
    })
    expect(done).to.equal(true)
  })
  it('describes any parameters the command has', () => {
    expect(withDecoyLogs(() => {
      new Context(new Command('foo')
        .parameter({
          name: 'bar',
          desc: 'Source bar.'
        })
        .parameter({
          name: 'boo',
          optional: true,
          desc: 'Destination boo.'
        })
        .parameter({
          name: 'baz',
          optional: true,
          variadic: true
        })
        .exit(null))
        .help()
    })).to.equal([
      'Usage: foo [OPTIONS...] <bar> [boo] [baz...]',
      'Parameters:',
      '  bar  Source bar.',
      '  boo  Destination boo.',
      '  baz',
      'Options:',
      '  -h,--help  You are here.'
    ].join('\n'))
  })
  it('describes any subcommands the command has', () => {
    expect(withDecoyLogs(() => {
      new Context(new Command('foo')
        .subcommand(new Command('bar').summary('Perform bar.'))
        .subcommand(new Command('boo').summary('Perform boo.'))
        .subcommand(new Command('baz'))
        .exit(null))
        .help()
    })).to.equal([
      'Usage: foo [OPTIONS...] bar|boo|baz',
      'Commands:',
      '  foo bar  Perform bar.',
      '  foo boo  Perform boo.',
      '  foo baz',
      'Options:',
      '  -h,--help  You are here.'
    ].join('\n'))
  })
  it('describes any options the command has', () => {
    expect(withDecoyLogs(() => {
      new Context(new Command('foo')
        .option({
          long: 'bar',
          desc: 'Enable bar.'
        })
        .option({
          long: 'boo',
          short: 'b',
          desc: 'Enable boo.'
        })
        .option({
          long: 'baz',
          short: 'B',
          value: 'BAZ',
          desc: 'Specify baz.'
        })
        .exit(null))
        .help()
    })).to.equal([
      'Usage: foo [OPTIONS...]',
      'Options:',
      '  -h,--help     You are here.',
      '     --bar      Enable bar.',
      '  -b,--boo      Enable boo.',
      '  -B,--baz=BAZ  Specify baz.'
    ].join('\n'))
  })
  it('describes itself if possible', () => {
    expect(withDecoyLogs(() => {
      new Context(new Command('foo')
        .summary('Performs foo with gusto.')
        .exit(null))
        .help()
    })).to.equal([
      'Usage: foo [OPTIONS...]',
      'Summary:',
      '  Performs foo with gusto.',
      'Options:',
      '  -h,--help  You are here.'
    ].join('\n'))
  })
})

describe('command.parse', () => {
  it('expects a handler to be set', () => {
    expect(() => {
      new Command().parse([])
    }).to.throw()
  })
  it('expects argument list to be an array', () => {
    forEachNonArray(true, (val) => {
      expect(() => {
        new Command().handler(() => {}).parse(val)
      }).to.throw()
    })
  })
  it('executes the command handler', () => {
    var handled = false
    parseTest(new Command()
      .handler(() => { handled = true }),
    [])
    expect(handled).to.equal(true)
  })
  it('passes context object to the handler', () => {
    var handled = false
    parseTest(new Command()
      .handler((ctx) => {
        expect(ctx).to.be.instanceof(Context)
        handled = true
      }),
    [])
    expect(handled).to.equal(true)
  })
  it('passes parameters to the handler', () => {
    var handled = false
    parseTest(new Command()
      .handler((ctx) => {
        expect(ctx.args).to.deep.equal(['foo', 'bar'])
        handled = true
      }),
    ['foo', 'bar'])
    expect(handled).to.equal(true)
  })
  it('passes options to the handler', () => {
    var handled = false
    parseTest(new Command()
      .option({ long: 'foo', short: 'f' })
      .option({ long: 'bar' })
      .handler((ctx) => {
        expect(ctx.args).to.deep.equal([])
        expect(ctx.opts).to.deep.equal({
          foo: true,
          bar: true
        })
        handled = true
      }),
    ['-f', '--bar'])
    expect(handled).to.equal(true)
  })
  it('passes option values to the handler', () => {
    var handled = false
    parseTest(new Command()
      .option({ long: 'foo', short: 'f', value: true })
      .option({ long: 'bar', short: 'b', value: true })
      .option({ long: 'boo', value: true })
      .option({ long: 'baz', value: true })
      .handler((ctx) => {
        expect(ctx.args).to.deep.equal([])
        expect(ctx.opts).to.deep.equal({
          foo: 'FOO',
          bar: 'BAR',
          boo: 'BOO',
          baz: 'BAZ'
        })
        handled = true
      }),
    ['-fFOO', '-b', 'BAR', '--boo=BOO', '--baz', 'BAZ'])
    expect(handled).to.equal(true)
  })
  it('rejects unknown options', () => {
    var handled = false
    parseTest(new Command('foo')
      .handler(() => {
        handled = true
      }),
    ['--bar'],
    1, [
      'foo: Unknown option \'--bar\'.',
      'foo: Type \'foo --help\' for more information.'
    ])
    expect(handled).to.equal(false)
  })
  it('rejects options missing required values', () => {
    var handled = false
    parseTest(new Command('foo')
      .option({ long: 'bar', value: true })
      .handler(() => {
        handled = true
      }),
    ['--bar'],
    1, [
      'foo: Option \'--bar\' requires a value.',
      'foo: Type \'foo --help\' for more information.'
    ])
    expect(handled).to.equal(false)
  })
  it('rejects options with unexpected values', () => {
    var handled = false
    parseTest(new Command('foo')
      .option({ long: 'bar' })
      .handler(() => {
        handled = true
      }),
    ['--bar=BAR'],
    1, [
      'foo: Option \'--bar\' does not take a value.',
      'foo: Type \'foo --help\' for more information.'
    ])
    expect(handled).to.equal(false)
  })
  it('collects repeated options when expected', () => {
    var handled = false
    parseTest(new Command()
      .option({ long: 'foo', value: false, repeatable: true })
      .option({ long: 'bar', value: true, repeatable: true })
      .option({ long: 'boo', value: false, repeatable: true })
      .option({ long: 'baz', value: true, repeatable: true })
      .handler((ctx) => {
        expect(ctx.args).to.deep.equal([])
        expect(ctx.opts).to.deep.equal({
          foo: 3,
          bar: ['one', 'two']
        })
        handled = true
      }),
    ['--foo', '--bar=one', '--foo', '--foo', '--bar=two'])
    expect(handled).to.equal(true)
  })
  it('rejects repeated options when unexpected', () => {
    var handled = false
    parseTest(new Command('foo')
      .option({ long: 'bar', repeatable: false })
      .handler(() => {
        handled = true
      }),
    ['--bar', '--bar'],
    1, [
      'foo: Option \'--bar\' cannot be used more than once.',
      'foo: Type \'foo --help\' for more information.'
    ])
    expect(handled).to.equal(false)
  })
  it('rejects long options in short form', () => {
    var handled = false
    parseTest(new Command('foo')
      .option({ long: 'bar', short: 'B' })
      .handler(() => {
        handled = true
      }),
    ['-bar'],
    1, [
      'foo: Unknown option \'-b\'.',
      'foo: Type \'foo --help\' for more information.'
    ])
    expect(handled).to.equal(false)
  })
  it('rejects short options in long form', () => {
    var handled = false
    parseTest(new Command('foo')
      .option({ long: 'bar', short: 'B' })
      .handler(() => {
        handled = true
      }),
    ['--B'],
    1, [
      'foo: Unknown option \'--B\'.',
      'foo: Type \'foo --help\' for more information.'
    ])
    expect(handled).to.equal(false)
  })
  it('collects named parameters', () => {
    var handled = false
    parseTest(new Command()
      .parameter({ name: 'foo' })
      .parameter({ name: 'bar' })
      .handler((ctx) => {
        expect(ctx.args).to.deep.equal(munge([], {
          foo: 'foo',
          bar: 'bar'
        }))
        handled = true
      }),
    ['foo', 'bar'])
    expect(handled).to.equal(true)
  })
  it('collects named variadic parameters', () => {
    var handled = false
    parseTest(new Command()
      .parameter({ name: 'foo' })
      .parameter({
        name: 'bar',
        optional: false,
        variadic: true
      })
      .handler((ctx) => {
        expect(ctx.args).to.deep.equal(munge([], {
          foo: 'foo',
          bar: ['bar', 'boo']
        }))
        handled = true
      }),
    ['foo', 'bar', 'boo'])
    expect(handled).to.equal(true)
  })
  it('collects leftover parameters', () => {
    var handled = false
    parseTest(new Command()
      .parameter({ name: 'foo' })
      .parameter({ name: 'bar' })
      .handler((ctx) => {
        expect(ctx.args).to.deep.equal(munge([
          'boo',
          'baz'
        ], {
          foo: 'foo',
          bar: 'bar'
        }))
        handled = true
      }),
    ['foo', 'bar', 'boo', 'baz'])
    expect(handled).to.equal(true)
  })
  it('allows missing optional parameters', () => {
    var handled = false
    parseTest(new Command()
      .parameter({
        name: 'foo',
        optional: true
      })
      .handler((ctx) => {
        expect(ctx.args).to.deep.equal([])
        handled = true
      }),
    [])
    expect(handled).to.equal(true)
  })
  it('rejects missing required parameters', () => {
    var handled = false
    parseTest(new Command('foo')
      .parameter({ name: 'bar' })
      .handler(() => {
        handled = true
      }),
    [],
    1, [
      'foo: Parameter \'bar\' is required.',
      'foo: Type \'foo --help\' for more information.'
    ])
    expect(handled).to.equal(false)
  })
  it('collects named environment variables', () => {
    var handled = false
    withDecoy('process.env', {
      FOO: 'foo',
      BAR: 'bar'
    }, () => {
      parseTest(new Command()
        .variable({ name: 'FOO' })
        .variable({ name: 'BAR' })
        .variable({ name: 'BOO' })
        .handler((ctx) => {
          expect(ctx.vars).to.deep.equal({
            FOO: 'foo',
            BAR: 'bar'
          })
          handled = true
        }))
    })
    expect(handled).to.equal(true)
  })
  it('rejects missing required environment variables', () => {
    var handled = false
    withDecoy('process.env', {}, () => {
      parseTest(new Command('foo')
        .variable({ name: 'BAR', required: true })
        .handler(() => {
          handled = true
        }),
      [],
      1, [
        'foo: Required variable \'BAR\' is not set.',
        'foo: Type \'foo --help\' for more information.'
      ])
    })
    expect(handled).to.equal(false)
  })
  it('substitutes default values for missing variables', () => {
    var handled = false
    withDecoy('process.env', {}, () => {
      parseTest(new Command('foo')
        .variable({ name: 'BAR', default: 'bar', required: true })
        .variable({ name: 'BOO', default: 'boo', required: false })
        .handler((ctx) => {
          expect(ctx.vars).to.deep.equal({
            BAR: 'bar',
            BOO: 'boo'
          })
          handled = true
        }))
    })
    expect(handled).to.equal(true)
  })
  it('executes subcommands', () => {
    var handled = false
    parseTest(new Command('foo')
      .subcommand(new Command('bar')
        .option({ long: 'boo' })
        .handler((ctx) => {
          expect(ctx.name).to.equal('bar')
          expect(ctx.path).to.equal('foo bar')
          expect(ctx.args).to.deep.equal(['baz'])
          expect(ctx.opts).to.deep.equal({ boo: true })
          handled = true
        })),
    ['bar', '--boo', 'baz'])
    expect(handled).to.equal(true)
  })
  it('rejects unknown subcommands', () => {
    var handled = false
    parseTest(new Command('foo')
      .subcommand(new Command('bar')
        .handler(() => {
          handled = true
        })),
    ['boo'],
    1, [
      'foo: Unknown subcommand \'boo\'.',
      'foo: Type \'foo --help\' for more information.'
    ])
    expect(handled).to.equal(false)
  })
  it('calls parent handler if subcommand is missing', () => {
    var handled1 = false
    var handled2 = false
    parseTest(new Command('foo')
      .handler(() => {
        handled1 = true
      })
      .subcommand(new Command('bar')
        .handler(() => {
          handled2 = true
        })),
    [])
    expect(handled1).to.equal(true)
    expect(handled2).to.equal(false)
  })
  it('rejects missing subcommand if parent command has no handler', () => {
    var handled = false
    parseTest(new Command('foo')
      .subcommand(new Command('bar')
        .handler(() => {
          handled = true
        })),
    [],
    1, [
      'foo: A subcommand is required.',
      'foo: Type \'foo --help\' for more information.'
    ])
    expect(handled).to.equal(false)
  })
  it('recognises options inherited by subcommands', () => {
    var handled = false
    parseTest(new Command()
      .option({ long: 'foo', inherit: true })
      .subcommand(new Command('bar')
        .option({ long: 'boo' })
        .handler((ctx) => {
          expect(ctx.opts).to.deep.equal({
            foo: true,
            boo: true
          })
          handled = true
        })),
    ['bar', '--foo', '--boo'])
    expect(handled).to.equal(true)
  })
  it('recognises variables inherited by subcommands', () => {
    var handled = false
    withDecoy('process.env', {
      FOO: 'foo',
      BOO: 'boo'
    }, () => {
      parseTest(new Command()
        .variable({ name: 'FOO', inherit: true })
        .subcommand(new Command('bar')
          .variable({ name: 'BOO' })
          .handler((ctx) => {
            expect(ctx.vars).to.deep.equal({
              FOO: 'foo',
              BOO: 'boo'
            })
            handled = true
          })),
      ['bar'])
    })
    expect(handled).to.equal(true)
  })
  it('parses process.argv if no array is provided', () => {
    var handled = false
    withDecoy('process.argv', [
      'foo',
      'bar',
      'boo',
      'baz'
    ], () => {
      parseTest(new Command()
        .handler((ctx) => {
          expect(ctx.args).to.deep.equal([
            'boo',
            'baz'
          ])
          handled = true
        }), null)
    })
    expect(handled).to.equal(true)
  })
  it('provides \'--help\' automatically', () => {
    var handled = false
    parseTest(new Command('foo')
      .option({ long: 'bar', short: 'b', value: 'BAR', desc: 'Specify bar.' })
      .option({ long: 'boo', value: 'BOO', desc: 'Specify boo.' })
      .option({ long: 'baz', desc: 'Enable baz.' })
      .parameter({
        name: 'bar',
        desc: 'Source bar.'
      })
      .parameter({
        name: 'boo',
        optional: true,
        desc: 'Destination boo.'
      })
      .parameter({
        name: 'baz',
        optional: true,
        variadic: true,
        desc: 'Additional baz.'
      })
      .variable({ name: 'BAR', desc: 'Environment bar.' })
      .variable({ name: 'BOO', desc: 'Environment boo.' })
      .variable({ name: 'BAZ', desc: 'Environment baz.' })
      .handler(() => {
        handled = true
      }),
    ['--help'],
    0, [], [
      'Usage: foo [OPTIONS...] <bar> [boo] [baz...]',
      'Parameters:',
      '  bar  Source bar.',
      '  boo  Destination boo.',
      '  baz  Additional baz.',
      'Options:',
      '  -h,--help     You are here.',
      '  -b,--bar=BAR  Specify bar.',
      '     --boo=BOO  Specify boo.',
      '     --baz      Enable baz.',
      'Variables:',
      '  BAR  Environment bar.',
      '  BOO  Environment boo.',
      '  BAZ  Environment baz.'
    ])
    expect(handled).to.equal(false)
  })
  it('executes any installed middleware', () => {
    var handled1 = false
    var handled2 = false
    parseTest(new Command()
      .middleware((ctx, next) => {
        handled1 = true
        next()
      })
      .handler(() => { handled2 = true }),
    [])
    expect(handled1).to.equal(true)
    expect(handled2).to.equal(true)
  })
  it('executes middleware inherited from parent commands', () => {
    var handled1 = false
    var handled2 = false
    var handled3 = false
    parseTest(new Command()
      .middleware((ctx, next) => {
        handled1 = true
        next()
      })
      .subcommand(new Command('foo')
        .middleware((ctx, next) => {
          handled2 = true
          next()
        })
        .handler(() => { handled3 = true })),
    ['foo'])
    expect(handled1).to.equal(true)
    expect(handled2).to.equal(true)
    expect(handled3).to.equal(true)
  })
  it('collects all arguments after a \'--\' as leftovers', () => {
    var handled = false
    parseTest(new Command()
      .parameter({ name: 'foo' })
      .parameter({ name: 'bar', variadic: true })
      .option({ long: 'boo' })
      .option({ long: 'baz' })
      .handler((ctx) => {
        expect(ctx.args).to.deep.equal(munge([
          'boo', '--baz', 'baz'
        ], {
          foo: 'foo',
          bar: ['bar1', 'bar2', 'bar3']
        }))
        expect(ctx.opts).to.deep.equal({
          boo: true
        })
        handled = true
      }),
    ['foo', '--boo', 'bar1', 'bar2', 'bar3', '--', 'boo', '--baz', 'baz'])
    expect(handled).to.equal(true)
  })
})

function deepGet (name) {
  return name.split('.').reduce((acc, key) => {
    return acc[key]
  }, global)
}

function deepSet (name, val) {
  return name.split('.').reduce((acc, key, idx, arr) => {
    if (idx < arr.length - 1) {
      return acc[key]
    }
    acc[key] = val
  }, global)
}

function withDecoy (name, val, fn) {
  var done = false
  var orig = deepGet(name)
  deepSet(name, val || (() => {
    done = true
  }))
  try {
    fn()
  } finally {
    deepSet(name, orig)
  }
  return done
}

function withDecoyLogs (fn) {
  var logs = []
  withDecoy('console.log', (msg) => {
    logs.push(msg)
  }, fn)
  return logs.join('\n')
}

function parseTest (command, argv, code, errors, logs) {
  var _code
  var _errors = []
  var _logs = withDecoyLogs(() => {
    command
      .error((msg) => _errors.push(msg))
      .exit((code) => { _code = code })
      .parse(argv)
  })
  if (code == null) {
    expect(_code).to.equal(undefined)
  } else {
    expect(_code).to.equal(code)
  }
  if (errors) {
    expect(_errors.join('\n')).to.equal(errors.join('\n'))
  } else {
    expect(_errors).to.be.empty
  }
  if (logs) {
    expect(_logs).to.equal(logs.join('\n'))
  } else {
    expect(_logs).to.be.empty
  }
}

function munge (arr, obj) {
  Object.keys(obj).forEach((key) => {
    arr[key] = obj[key]
  })
  return arr
}

function forEachPrimitive (opt, fn) {
  [undefined, null, 0, 1, true, false, [], {}, () => {}, 'foo']
    .slice(opt ? 2 : 0)
    .forEach(fn)
}

function forEachNonStr (opt, fn) {
  [undefined, null, 0, 1, true, false, [], {}, () => {}]
    .slice(opt ? 2 : 0)
    .forEach(fn)
}

function forEachNonFn (opt, fn) {
  [undefined, null, 0, 1, true, false, [], {}, 'foo']
    .slice(opt ? 2 : 0)
    .forEach(fn)
}

function forEachNonArray (opt, fn) {
  [undefined, null, 0, 1, true, false, {}, () => {}, 'foo']
    .slice(opt ? 2 : 0)
    .forEach(fn)
}
