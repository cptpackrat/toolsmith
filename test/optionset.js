'use strict'

/* eslint-disable no-new */
/* eslint-disable no-unused-expressions */

const util = require('./util.js')
const chai = require('chai')
const expect = chai.expect

const OptionSet = require('../lib/optionset.js')

describe('optionset', () => {
  describe('push', () => {
    it('accepts option details as an object', () => {
      var set = new OptionSet()
      set.push({
        long: 'foo',
        short: 'f',
        value: true,
        inherit: false,
        repeatable: false,
        desc: 'Foo.'
      })
      expect(set[0]).to.deep.equal({
        key: 'foo',
        long: 'foo',
        short: 'f',
        value: true,
        inherit: false,
        repeatable: false,
        desc: 'Foo.'
      })
    })
    it('unrecognised fields are ignored', () => {
      var set = new OptionSet()
      set.push({
        long: 'foo',
        short: 'f',
        herp: 'herp',
        value: true,
        inherit: false,
        repeatable: false,
        desc: 'Foo.',
        derp: 'derp'
      })
      expect(set[0]).to.deep.equal({
        key: 'foo',
        long: 'foo',
        short: 'f',
        value: true,
        inherit: false,
        repeatable: false,
        desc: 'Foo.'
      })
    })
    it('requires a long option', () => {
      var set = new OptionSet()
      expect(() => {
        set.push({})
      }).to.throw()
    })
    it('requires only a long option', () => {
      var set = new OptionSet()
      set.push({ long: 'foo' })
    })
    it('expects long option to be a string', () => {
      var set = new OptionSet()
      util.forEachNonStr(false, (val) => {
        expect(() => {
          set.push({ long: val })
        }).to.throw()
      })
    })
    it('expects long option to be at least two characters long', () => {
      var set = new OptionSet()
      expect(() => {
        set.push({ long: '' })
      }).to.throw()
      expect(() => {
        set.push({ long: 'f' })
      }).to.throw()
    })
    it('expects long option to be unique', () => {
      var set = new OptionSet()
      set.push({ long: 'foo' })
      expect(() => {
        set.push({ long: 'foo' })
      }).to.throw()
    })
    it('expects short option to be a string', () => {
      var set = new OptionSet()
      util.forEachNonStr(true, (val) => {
        expect(() => {
          set.push({ long: 'foo', short: val })
        }).to.throw()
      })
    })
    it('expects short option to be exactly one character long', () => {
      var set = new OptionSet()
      expect(() => {
        set.push({ long: 'foo', short: '' })
      }).to.throw()
      expect(() => {
        set.push({ long: 'foo', short: 'ff' })
      }).to.throw()
    })
    it('expects short option to be unique', () => {
      var set = new OptionSet()
      set.push({ short: 'f', long: 'foo' })
      expect(() => {
        set.push({ short: 'f', long: 'bar' })
      }).to.throw()
    })
    it('expects description to be a string', () => {
      var set = new OptionSet()
      util.forEachNonStr(true, (val) => {
        expect(() => {
          set.push({ long: 'foo', short: val })
        }).to.throw()
      })
    })
    it('coerces \'inherit\' and \'repeatable\' into booleans', () => {
      var set = new OptionSet()
      set.push({
        long: 'foo',
        inherit: 1
      })
      expect(set[0].inherit).to.equal(true)
      expect(set[0].repeatable).to.equal(false)
    })
    it('coerces \'value\' into a boolean if it is not a string', () => {
      var set = new OptionSet()
      set.push({ long: 'foo' })
      set.push({ long: 'bar', value: 1 })
      set.push({ long: 'boo', value: 'BOO' })
      expect(set[0].value).to.equal(false)
      expect(set[1].value).to.equal(true)
      expect(set[2].value).to.equal('BOO')
    })
    it('uses custom key if one is provided', () => {
      var set = new OptionSet()
      set.push({
        key: 'bar',
        long: 'foo'
      })
      expect(set[0].key).to.equal('bar')
      expect(set[0].long).to.equal('foo')
    })
    it('expects key to be a string', () => {
      var set = new OptionSet()
      util.forEachNonStr(true, (val) => {
        expect(() => {
          set.push({ long: 'foo', key: val })
        }).to.throw()
      })
    })
    it('expects key to be unique', () => {
      var set = new OptionSet()
      set.push({ key: 'foo', long: 'foo' })
      expect(() => {
        set.push({ key: 'foo', long: 'bar' })
      }).to.throw()
    })
  })
  describe('clone', () => {
    it('returns an exact copy of the set', () => {
      var set = new OptionSet()
      set.push({ long: 'foo', short: 'f' })
      set.push({ long: 'bar', short: 'b' })
      expect(set.clone()).to.not.equal(set)
      expect(set.clone()).to.deep.equal(set)
    })
  })
  describe('inherit', () => {
    it('appends the contents of a parent set', () => {
      var opt1 = { long: 'foo', inherit: true }
      var opt2 = { long: 'bar', inherit: true }
      var set1 = new OptionSet()
      var set2 = new OptionSet()
      set1.push(opt1)
      set2.push(opt2)
      set2.inherit(set1)
      var exp = new OptionSet()
      exp.push(opt2)
      exp.push(opt1)
      expect(set2).to.deep.equal(exp)
    })
    it('skips options that are not marked as inheritable', () => {
      var opt1 = { long: 'foo', inherit: false }
      var opt2 = { long: 'bar', inherit: true }
      var set1 = new OptionSet()
      var set2 = new OptionSet()
      set1.push(opt1)
      set1.push(opt2)
      set2.inherit(set1)
      var exp = new OptionSet()
      exp.push(opt2)
      expect(set2).to.deep.equal(exp)
    })
    it('skips options that conflict with existing long options', () => {
      var opt1 = { long: 'foo', inherit: true }
      var opt2 = { long: 'foo', inherit: true }
      var set1 = new OptionSet()
      var set2 = new OptionSet()
      set1.push(opt1)
      set2.push(opt2)
      set2.inherit(set1)
      var exp = new OptionSet()
      exp.push(opt2)
      expect(set2).to.deep.equal(exp)
    })
    it('skips options that conflict with existing short options', () => {
      var opt1 = { long: 'foo', short: 'f', inherit: true }
      var opt2 = { long: 'bar', short: 'f', inherit: true }
      var set1 = new OptionSet()
      var set2 = new OptionSet()
      set1.push(opt1)
      set2.push(opt2)
      set2.inherit(set1)
      var exp = new OptionSet()
      exp.push(opt2)
      expect(set2).to.deep.equal(exp)
    })
    it('skips options that conflict with existing option keys', () => {
      var opt1 = { key: 'foo', long: 'foo', inherit: true }
      var opt2 = { key: 'foo', long: 'bar', inherit: true }
      var set1 = new OptionSet()
      var set2 = new OptionSet()
      set1.push(opt1)
      set2.push(opt2)
      set2.inherit(set1)
      var exp = new OptionSet()
      exp.push(opt2)
      expect(set2).to.deep.equal(exp)
    })
  })
  describe('usage', () => {
    it('returns a fixed string because lazy', () => {
      expect(new OptionSet().usage()).to.equal('[OPTIONS...]')
    })
  })
  describe('help', () => {
    it('displays long options', () => {
      var set = new OptionSet()
      set.push({ long: 'foo' })
      expect(set.help()).to.equal([
        'Options:',
        '  --foo'
      ].join('\n'))
    })
    it('displays short options if present', () => {
      var set = new OptionSet()
      set.push({ long: 'foo', short: 'f' })
      expect(set.help()).to.equal([
        'Options:',
        '  -f,--foo'
      ].join('\n'))
    })
    it('pads long options for a consistent start location', () => {
      var set = new OptionSet()
      set.push({ long: 'foo', short: 'f' })
      set.push({ long: 'bar' })
      expect(set.help()).to.equal([
        'Options:',
        '  -f,--foo',
        '     --bar'
      ].join('\n'))
    })
    it('indicates whether an option receives a value', () => {
      var set = new OptionSet()
      set.push({ long: 'foo', short: 'f', value: true })
      set.push({ long: 'bar', value: true })
      expect(set.help()).to.equal([
        'Options:',
        '  -f,--foo=VALUE',
        '     --bar=VALUE'
      ].join('\n'))
    })
    it('displays value description if one is provided', () => {
      var set = new OptionSet()
      set.push({ long: 'foo', short: 'f', value: 'FOO' })
      set.push({ long: 'bar', value: 'BAR' })
      expect(set.help()).to.equal([
        'Options:',
        '  -f,--foo=FOO',
        '     --bar=BAR'
      ].join('\n'))
    })
    it('displays option description if one is provided', () => {
      var set = new OptionSet()
      set.push({ long: 'foo', short: 'f', desc: 'Enable foo.' })
      expect(set.help()).to.equal([
        'Options:',
        '  -f,--foo  Enable foo.'
      ].join('\n'))
    })
    it('pads descriptions for a consistent start location', () => {
      var set = new OptionSet()
      set.push({ long: 'foo', short: 'f', desc: 'Enable foo.' })
      set.push({ long: 'bar', desc: 'Enable bar.' })
      expect(set.help()).to.equal([
        'Options:',
        '  -f,--foo  Enable foo.',
        '     --bar  Enable bar.'
      ].join('\n'))
      set.push({
        long: 'boo',
        short: 'b',
        value: 'BOO',
        desc: 'Specify custom boo.'
      })
      expect(set.help()).to.equal([
        'Options:',
        '  -f,--foo      Enable foo.',
        '     --bar      Enable bar.',
        '  -b,--boo=BOO  Specify custom boo.'
      ].join('\n'))
      set.push({
        long: 'baz',
        value: true,
        desc: 'Specify custom baz.'
      })
      expect(set.help()).to.equal([
        'Options:',
        '  -f,--foo        Enable foo.',
        '     --bar        Enable bar.',
        '  -b,--boo=BOO    Specify custom boo.',
        '     --baz=VALUE  Specify custom baz.'
      ].join('\n'))
    })
  })
})
