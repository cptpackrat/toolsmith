'use strict'

const assert = require('assert')
const {
  isObj,
  isStr,
  isNone
} = require('./util.js')

module.exports = class OptionSet extends Array {
  constructor () {
    super()
    this.short = {}
    this.long = {}
    this.keys = {}
  }

  push (opts) {
    assert(isObj(opts), 'expected option details')
    assert(isStr(opts.long), 'long option must be a string')
    assert(isStr(opts.key, true), 'option key must be a string')
    assert(isStr(opts.desc, true), 'option description must be a string')
    assert(isStr(opts.short, true), 'short option must be a string')
    assert(opts.long.length > 1, 'long option must be at least 2 characters')
    if (isStr(opts.short)) {
      assert(opts.short.length === 1, 'short option must be exactly 1 character')
    }
    this._push({
      long: opts.long,
      desc: opts.desc || null,
      short: opts.short || null,
      inherit: !!opts.inherit,
      repeatable: !!opts.repeatable,
      value: isStr(opts.value)
        ? opts.value
        : !!opts.value,
      key: isNone(opts.key)
        ? opts.long
        : opts.key
    })
  }

  _push (opt) {
    assert(!this.short[opt.short], 'short option \'' + opt.short + '\' already exists')
    assert(!this.long[opt.long], 'long option \'' + opt.long + '\' already exists')
    assert(!this.keys[opt.key], 'option key \'' + opt.key + '\' already exists')
    super.push(opt)
    this.keys[opt.key] = opt
    this.long[opt.long] = opt
    if (opt.short) {
      this.short[opt.short] = opt
    }
  }

  clone () {
    var clone = new OptionSet()
    this.forEach((opt) => {
      clone._push(opt)
    })
    return clone
  }

  inherit (set) {
    set.forEach((opt) => {
      if (opt.inherit && !this.keys[opt.key] && !this.long[opt.long] && !this.short[opt.short]) {
        this._push(opt)
      }
    })
  }

  usage () {
    return '[OPTIONS...]'
  }

  help () {
    var pad = false
    var min = 0
    var max = 0
    var arr = this.map((opt) => {
      var str = this._help(opt)
      min = Math.max(min, str.length)
      max = Math.max(max, opt.short ? str.length : str.length + 3)
      if (opt.short) {
        pad = true
      }
      return {
        str: str,
        obj: opt
      }
    })
    if (!pad) {
      max = min
    }
    return arr.reduce((acc, opt) => {
      return pad && !opt.obj.short
        ? opt.obj.desc
          ? acc + '\n     ' + opt.str.padEnd(max - 1) + opt.obj.desc
          : acc + '\n     ' + opt.str
        : opt.obj.desc
          ? acc + '\n  ' + opt.str.padEnd(max + 2) + opt.obj.desc
          : acc + '\n  ' + opt.str
    }, 'Options:')
  }

  _help (opt) {
    var str = opt.short
      ? '-' + opt.short + ',--' + opt.long
      : '--' + opt.long
    return opt.value
      ? isStr(opt.value)
        ? str + '=' + opt.value
        : str + '=VALUE'
      : str
  }
}
