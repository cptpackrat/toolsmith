'use strict'

const { basename } = require('path')
const { format } = require('util')
const {
  iterator,
  isStr,
  isFn,
  isArr
} = require('./util.js')

const assert = require('assert')
const OptionSet = require('./optionset.js')
const ParameterSet = require('./parameterset.js')
const VariableSet = require('./variableset.js')

exports.Command = class Command {
  constructor (name) {
    assert(isStr(name, true), 'command name must be a string')
    this._name = name
    this._func = null
    this._args = new ParameterSet()
    this._subs = new CommandSet()
    this._vars = new VariableSet()
    this._mids = []
    this._opts = new OptionSet()
    this._opts.push({
      key: 'help',
      long: 'help',
      short: 'h',
      value: false,
      inherit: true,
      desc: 'You are here.'
    })
  }

  variable (opts) {
    this._vars.push(opts)
    return this
  }

  option (opts) {
    this._opts.push(opts)
    return this
  }

  parameter (opts) {
    assert(this._subs.length === 0, 'parameters cannot be combined with subcommands')
    this._args.push(opts)
    return this
  }

  subcommand (cmd) {
    assert(cmd instanceof Command, 'subcommand must be a command object')
    assert(this._args.length === 0, 'subcommands cannot be combined with parameters')
    this._subs.push(cmd)
    return this
  }

  summary (desc) {
    assert(isStr(desc), 'command description must be a string')
    this._desc = desc
    return this
  }

  handler (fn) {
    assert(isFn(fn), 'handler must be a function')
    this._func = fn
    return this
  }

  error (fn) {
    assert(isFn(fn, true), 'handler must be a function')
    this._error = fn
    return this
  }

  exit (fn) {
    assert(isFn(fn, true), 'handler must be a function')
    this._exit = fn
    return this
  }

  middleware (fn) {
    assert(isFn(fn), 'handler must be a function')
    this._mids.push(fn)
    return this
  }

  install (fn) {
    assert(isFn(fn), 'expected a function')
    fn(this)
    return this
  }

  parse (argv) {
    assert(isArr(argv, true), 'expected arguments list to be an array')
    if (!argv) {
      argv = process.argv.slice(2)
    }
    var end = false
    argv = argv.map((arg) => {
      const modes = [{
        regex: /^-([a-zA-Z0-9])$/,
        type: 'short',
        str: 0,
        opt: 1,
        val: null
      }, {
        regex: /^(-([a-zA-Z0-9]))(.+)$/,
        type: 'short',
        str: 1,
        opt: 2,
        val: 3
      }, {
        regex: /^--([a-zA-Z0-9][a-zA-Z0-9-]*)$/,
        type: 'long',
        str: 0,
        opt: 1,
        val: null
      }, {
        regex: /^(--([a-zA-Z0-9][a-zA-Z0-9-]*))=(.*)$/,
        type: 'long',
        str: 1,
        opt: 2,
        val: 3
      }]
      if (!end) {
        for (var mode in modes) {
          mode = modes[mode]
          var match = arg.match(mode.regex)
          if (match) {
            return {
              type: mode.type,
              str: match[mode.str],
              opt: match[mode.opt],
              val: (mode.val ? match[mode.val] : null)
            }
          }
        }
        if (arg === '--') {
          end = true
          return {
            type: 'end'
          }
        }
      }
      return {
        type: 'arg',
        str: arg
      }
    })
    this._parse(new Context(this), argv)
  }

  _parse (ctx, argv) {
    var parmIt = iterator(this._args)
    var argIt = iterator(argv)
    for (var arg = argIt.get(); arg; arg = argIt.next()) {
      if (arg.type === 'arg') {
        if (this._subs.length > 0) {
          var sub = this._subs.name[arg.str]
          if (sub) {
            return sub._parse(ctx._push(sub), argIt.slice())
          } else {
            return ctx.usage('Unknown subcommand \'%s\'.', arg.str)
          }
        } else {
          var parm = parmIt.get()
          if (parm) {
            if (parm.variadic) {
              if (!ctx.args[parm.key]) {
                ctx.args[parm.key] = [arg.str]
              } else {
                ctx.args[parm.key].push(arg.str)
              }
            } else {
              ctx.args[parm.key] = arg.str
              parmIt.next()
            }
          } else {
            ctx.args.push(arg.str)
          }
        }
      } else if (arg.type === 'end') {
        for (arg = argIt.next(); arg; arg = argIt.next()) {
          ctx.args.push(arg.str)
        }
        break
      } else {
        var opt = arg.type === 'short'
          ? ctx._opts.short[arg.opt]
          : ctx._opts.long[arg.opt]
        if (!opt) {
          return ctx.usage('Unknown option \'%s\'.', arg.str)
        }
        if (!opt.value && arg.val) {
          return ctx.usage('Option \'%s\' does not take a value.', arg.str)
        }
        if (opt.value && !arg.val) {
          var val = argIt.next()
          if (!val) {
            return ctx.usage('Option \'%s\' requires a value.', arg.str)
          }
          arg.val = val.str
        }
        if (opt.repeatable) {
          if (opt.value) {
            if (!ctx.opts[opt.key]) {
              ctx.opts[opt.key] = [arg.val]
            } else {
              ctx.opts[opt.key].push(arg.val)
            }
          } else {
            if (!ctx.opts[opt.key]) {
              ctx.opts[opt.key] = 1
            } else {
              ctx.opts[opt.key]++
            }
          }
        } else {
          if (ctx.opts[opt.key]) {
            return ctx.usage('Option \'%s\' cannot be used more than once.', arg.str)
          }
          ctx.opts[opt.key] = (opt.value ? arg.val : true)
        }
      }
    }
    if (ctx.opts.help) {
      return ctx.help()
    }
    if (!this._func && this._subs.length > 0) {
      return ctx.usage('A subcommand is required.')
    }
    var prev = parmIt.get()
    if (prev && !prev.optional && !ctx.args[prev.key]) {
      return ctx.usage('Parameter \'%s\' is required.', prev.name)
    }
    var envIt = iterator(ctx._vars)
    for (var env = envIt.get(); env; env = envIt.next()) {
      var ev = process.env[env.name] || env.default
      if (ev) {
        ctx.vars[env.key] = ev
      } else if (env.required) {
        return ctx.usage('Required variable \'%s\' is not set.', env.name)
      }
    }
    assert(this._func, 'no handler function')
    ctx._exec(this._func)
  }
}

const CommandSet = exports.CommandSet = class CommandSet extends Array {
  constructor () {
    super()
    this.name = {}
  }

  push (cmd) {
    assert(isStr(cmd._name), 'subcommand must have a name')
    assert(!this.name[cmd._name], 'subcommand \'' + cmd._name + '\' already exists')
    super.push(cmd)
    this.name[cmd._name] = cmd
  }

  usage () {
    return this.reduce((acc, cmd) => {
      return acc
        ? acc + '|' + cmd._name
        : cmd._name
    }, null)
  }

  help (ctx) {
    var prefix = ctx
      ? '\n  ' + ctx.path + ' '
      : '\n  '
    var max = this.reduce((max, cmd) => {
      return Math.max(max, cmd._name.length)
    }, 0)
    return this.reduce((acc, cmd) => {
      return cmd._desc
        ? acc + prefix + cmd._name.padEnd(max + 2) + cmd._desc
        : acc + prefix + cmd._name
    }, 'Commands:')
  }
}

const Context = exports.Context = class Context {
  constructor (cmd) {
    this.args = []
    this.opts = {}
    this.vars = {}
    this._error = console.error
    this._exit = process.exit
    this._mids = []
    this._push(cmd)
  }

  error (fmt, ...args) {
    if (this._error) {
      this._error(format('%s: ' + fmt, this.base, ...args))
    }
  }

  exit (code) {
    if (this._exit) {
      this._exit(code || 0)
    }
  }

  fatal (code, ...args) {
    if (typeof code === 'number') {
      this.error(...args)
    } else {
      this.error(code, ...args)
      code = 1
    }
    this.exit(code)
  }

  usage (code, ...args) {
    if (typeof code === 'number') {
      this.error(...args)
    } else {
      this.error(code, ...args)
      code = 1
    }
    this.error('Type \'%s --help\' for more information.', this.path)
    this.exit(code)
  }

  help () {
    console.log(this._help())
    this.exit(0)
  }

  _push (cmd) {
    this.name = cmd._name || basename(process.argv[1])
    this.path = this.path
      ? this.path + ' ' + this.name
      : this.name
    if (!this.base) {
      this.base = this.name
    }
    var opts = cmd._opts.clone()
    if (this._opts) {
      opts.inherit(this._opts)
    }
    this._opts = opts
    var vars = cmd._vars.clone()
    if (this._vars) {
      vars.inherit(this._vars)
    }
    this._vars = vars
    this._mids = this._mids.concat(cmd._mids)
    if (cmd._error !== undefined) {
      this._error = cmd._error
    }
    if (cmd._exit !== undefined) {
      this._exit = cmd._exit
    }
    this._cmd = cmd
    return this
  }

  _exec (fn) {
    var ctx = this
    var it = iterator(this._mids.concat(fn))
    function next () {
      var fn = it.get()
      if (it.next()) {
        fn(ctx, next)
      } else {
        fn(ctx)
      }
    }
    next()
  }

  _usage () {
    return [
      this._opts,
      this._cmd._subs,
      this._cmd._args
    ].reduce((acc, obj) => {
      return obj.length > 0
        ? acc + ' ' + obj.usage(this)
        : acc
    }, 'Usage: ' + this.path)
  }

  _help () {
    return [
      this._cmd._subs,
      this._cmd._args,
      this._opts,
      this._vars
    ].reduce((acc, obj) => {
      return obj.length > 0
        ? acc + '\n' + obj.help(this)
        : acc
    }, this._cmd._desc
      ? this._usage() + '\nSummary:\n  ' + this._cmd._desc
      : this._usage())
  }
}
