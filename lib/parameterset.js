'use strict'

const assert = require('assert')
const {
  isObj,
  isStr,
  isNone
} = require('./util.js')

module.exports = class ParameterSet extends Array {
  constructor () {
    super()
    this.keys = {}
  }

  push (opts) {
    assert(isObj(opts), 'expected parameter details')
    assert(isStr(opts.name), 'parameter name must be a string')
    assert(isStr(opts.key, true), 'parameter key must be a string')
    assert(isStr(opts.desc, true), 'parameter description must be a string')
    this._push({
      name: opts.name,
      desc: opts.desc || null,
      optional: !!opts.optional,
      variadic: !!opts.variadic,
      key: isNone(opts.key)
        ? opts.name
        : opts.key
    })
  }

  _push (parm) {
    assert(!this.keys[parm.key], 'parameter key \'' + parm.key + '\' already exists')
    var prev = this[this.length - 1]
    if (prev) {
      assert(!prev.variadic, 'previous parameter is variadic')
      if (prev.optional) {
        assert(parm.optional, 'previous parameter is optional, cannot follow with required')
      }
    }
    super.push(parm)
    this.keys[parm.key] = parm
  }

  usage () {
    return this.reduce((acc, parm) => {
      return acc
        ? acc + ' ' + this._usage(parm)
        : this._usage(parm)
    }, null)
  }

  _usage (parm) {
    return parm.optional
      ? parm.variadic
        ? '[' + parm.name + '...]'
        : '[' + parm.name + ']'
      : parm.variadic
        ? '<' + parm.name + '...>'
        : '<' + parm.name + '>'
  }

  help () {
    var max = this.reduce((max, parm) => {
      return Math.max(max, parm.name.length)
    }, 0)
    return this.reduce((acc, parm) => {
      return parm.desc
        ? acc + '\n  ' + parm.name.padEnd(max + 2) + parm.desc
        : acc + '\n  ' + parm.name
    }, 'Parameters:')
  }
}
