'use strict'

module.exports = {
  iterator: iterator,
  isNone: isNone,
  isObj: isObj,
  isArr: isArr,
  isStr: isStr,
  isFn: isFn
}

function iterator (arr) {
  var idx = 0
  return {
    get: () => arr[idx],
    next: () => arr[++idx],
    slice: () => arr.slice(idx + 1)
  }
}

function isObj (val) {
  return !isNone(val) && typeof (val) === 'object'
}

function isArr (val, opt) {
  return (opt && isNone(val)) || Array.isArray(val)
}

function isStr (val, opt) {
  return (opt && isNone(val)) || typeof (val) === 'string'
}

function isFn (val, opt) {
  return (opt && isNone(val)) || typeof (val) === 'function'
}

function isNone (val) {
  return val === null || val === undefined
}
