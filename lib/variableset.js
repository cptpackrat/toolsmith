'use strict'

const assert = require('assert')
const {
  isObj,
  isStr,
  isNone
} = require('./util.js')

module.exports = class VariableSet extends Array {
  constructor () {
    super()
    this.names = {}
    this.keys = {}
  }

  push (opts) {
    assert(isObj(opts), 'expected variable details')
    assert(isStr(opts.name), 'variable name must be a string')
    assert(isStr(opts.key, true), 'variable key must be a string')
    assert(isStr(opts.desc, true), 'variable description must be a string')
    assert(isStr(opts.default, true), 'variable default value must be a string')
    this._push({
      name: opts.name,
      desc: opts.desc || null,
      default: opts.default || null,
      required: !!opts.required,
      inherit: !!opts.inherit,
      key: isNone(opts.key)
        ? opts.name
        : opts.key
    })
  }

  _push (env) {
    assert(!this.names[env.name], 'variable name \'' + env.name + '\' already exists')
    assert(!this.keys[env.key], 'variable key \'' + env.key + '\' already exists')
    super.push(env)
    this.keys[env.key] = env
    this.names[env.name] = env
  }

  clone () {
    var clone = new VariableSet()
    this.forEach((env) => {
      clone._push(env)
    })
    return clone
  }

  inherit (set) {
    set.forEach((env) => {
      if (env.inherit && !this.keys[env.key] && !this.names[env.name]) {
        this._push(env)
      }
    })
  }

  help () {
    var max = this.reduce((max, env) => {
      return Math.max(max, env.name.length)
    }, 0)
    return this.reduce((acc, env) => {
      return env.desc
        ? acc + '\n  ' + env.name.padEnd(max + 2) + env.desc
        : acc + '\n  ' + env.name
    }, 'Variables:')
  }
}
