![Toolsmith][logo] Toolsmith
======
[![npm version][npm-image]][npm-url]
[![pipeline status][pipeline-image]][pipeline-url]
[![coverage status][coverage-image]][coverage-url]
[![standard-js][standard-image]][standard-url]
[![conventional commits][commits-image]][commits-url]

Yet another CLI tool framework.

## Installation

```
npm install toolsmith
```

## Basic Example
```js
#!/usr/bin/env node
require('toolsmith')()
  .summary('An example command.')
  .option({
    long: 'foo',
    desc: 'Enable foo.'
  })
  .parameter({
    name: 'bar',
    desc: 'Specify one or more values for bar.',
    variadic: true
  })
  .handler((ctx) => {
    if (ctx.opts.foo) {
      console.log('foo is enabled')
    }
    console.log('bar is ' + ctx.args.bar.join(', '))
  })
  .parse()
```

```
$ ./example.js --help
Usage: example.js [OPTIONS...] <bar...>
Summary:
  An example command.
Parameters:
  bar  Specify one or more values for bar.
Options:
  -h,--help  You are here.
     --foo   Enable foo.

$ ./example.js --foo some example values
foo is enabled
bar is some, example, values
```

## Documentation

API documentation can be found [here](https://gitlab.com/cptpackrat/toolsmith/blob/master/doc/api.md).

[logo]: https://gitlab.com/cptpackrat/toolsmith/raw/master/doc/logo.png
[npm-image]: https://img.shields.io/npm/v/toolsmith.svg
[npm-url]: https://www.npmjs.com/package/toolsmith
[pipeline-image]: https://gitlab.com/cptpackrat/toolsmith/badges/master/pipeline.svg
[pipeline-url]: https://gitlab.com/cptpackrat/toolsmith/commits/master
[coverage-image]: https://gitlab.com/cptpackrat/toolsmith/badges/master/coverage.svg
[coverage-url]: https://gitlab.com/cptpackrat/toolsmith/commits/master
[standard-image]: https://img.shields.io/badge/code%20style-standard-brightgreen.svg
[standard-url]: http://standardjs.com/
[commits-image]: https://img.shields.io/badge/conventional%20commits-1.0.0-yellow.svg
[commits-url]: https://www.conventionalcommits.org
