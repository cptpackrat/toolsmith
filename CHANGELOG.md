# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.5.2](https://gitlab.com/cptpackrat/toolsmith/compare/v0.5.1...v0.5.2) (2019-09-01)


### Bug Fixes

* Fixed outdated package manifest. ([8cf1c47](https://gitlab.com/cptpackrat/toolsmith/commit/8cf1c47))

### [0.5.1](https://gitlab.com/cptpackrat/toolsmith/compare/v0.5.0...v0.5.1) (2019-09-01)


### Improvements

* Split utilities, OptionSet, ParameterSet and VariableSet out into separate files. ([9a9bec1](https://gitlab.com/cptpackrat/toolsmith/commit/9a9bec1))
* Use standard assertions. ([04c8efc](https://gitlab.com/cptpackrat/toolsmith/commit/04c8efc))

## [0.5.0](https://gitlab.com/cptpackrat/toolsmith/compare/v0.4.0...v0.5.0) (2019-08-04)


### Bug Fixes

* Added missing length contraints for long and short options. ([803d86c](https://gitlab.com/cptpackrat/toolsmith/commit/803d86c))


### Features

* Added support for using '--' to terminate argument parsing. ([1e9da9f](https://gitlab.com/cptpackrat/toolsmith/commit/1e9da9f))


### Improvements

* Collapsed Option and OptionSet into a single class. ([d3134d2](https://gitlab.com/cptpackrat/toolsmith/commit/d3134d2))
* Collapsed Parameter and ParameterSet into a single class. ([63171c4](https://gitlab.com/cptpackrat/toolsmith/commit/63171c4))
* Collapsed Variable and VariableSet into a single class. ([468d08d](https://gitlab.com/cptpackrat/toolsmith/commit/468d08d))


### BREAKING CHANGES

* Command.option no longer accepts individual arguments, an object must be passed.
* Command.variable no longer accepts individual arguments, an object must be passed.
* Command.parameter no longer accepts individual arguments, an object must be passed.



## [0.4.0](https://gitlab.com/cptpackrat/toolsmith/compare/v0.3.0...v0.4.0) (2019-07-25)


### Bug Fixes

* Fixed type checking on optional parameters allowing falsy values. ([1fd460c](https://gitlab.com/cptpackrat/toolsmith/commit/1fd460c))


### Features

* Added support for collecting environment variables. ([4be42d5](https://gitlab.com/cptpackrat/toolsmith/commit/4be42d5))
* Added support for repeatable options. ([a5f3bf6](https://gitlab.com/cptpackrat/toolsmith/commit/a5f3bf6))


### Improvements

* Moved collected arguments and options inside context object. ([bb0968b](https://gitlab.com/cptpackrat/toolsmith/commit/bb0968b))
* Renamed 'global' option to 'inherit' and removed it as a discrete function parameter. ([6aa78be](https://gitlab.com/cptpackrat/toolsmith/commit/6aa78be))
* Renamed 'multiple' option to 'variadic'. ([b26635f](https://gitlab.com/cptpackrat/toolsmith/commit/b26635f))


### BREAKING CHANGES

* Arguments and options are now members of the context object, and not passed to handlers directly.
* The 'multiple' option is now called 'variadic'.
* The 'global' option is now called 'inherit', and must be specified in an options object.



## [0.3.0](https://gitlab.com/cptpackrat/toolsmith/compare/v0.2.0...v0.3.0) (2019-07-24)


### Features

* Added convenience function for installing subcommands and middleware via require. ([21e872a](https://gitlab.com/cptpackrat/toolsmith/commit/21e872a))
* Error logging functions now accept printf-style format strings. ([36980f9](https://gitlab.com/cptpackrat/toolsmith/commit/36980f9))


### Improvements

* Error messages are now prefixed with the base command name. ([4e731f0](https://gitlab.com/cptpackrat/toolsmith/commit/4e731f0))
* Removed support for custom stdout handling; auto-help now prints to console.log directly. ([36722ca](https://gitlab.com/cptpackrat/toolsmith/commit/36722ca))


### BREAKING CHANGES

* The error message for Context.fatal and Context.usage is no longer optional.
* The exit code for Context.fatal and Context.usage is now the first argument.
* Command.log and Context.log no longer exist.



## [0.2.0](https://gitlab.com/cptpackrat/toolsmith/compare/v0.1.1...v0.2.0) (2019-07-23)


### Features

* Middleware functions can now be installed to run before command handlers. ([c3a0ae1](https://gitlab.com/cptpackrat/toolsmith/commit/c3a0ae1))


### Improvements

* A single context object is now used to represent a command chain. ([5630174](https://gitlab.com/cptpackrat/toolsmith/commit/5630174))
* Added missing '-' and '--' to option error messages. ([ed26f6d](https://gitlab.com/cptpackrat/toolsmith/commit/ed26f6d))
* Call parent command handler if no subcommand is specified. ([dc98df8](https://gitlab.com/cptpackrat/toolsmith/commit/dc98df8))
* Show full sub-command paths in help output. ([a95a56e](https://gitlab.com/cptpackrat/toolsmith/commit/a95a56e))
* Usage errors now offer the '--help' flag instead of just dumping a usage string. ([f41736f](https://gitlab.com/cptpackrat/toolsmith/commit/f41736f))


### BREAKING CHANGES

* Context.name and Context.path are now strings rather than functions.
